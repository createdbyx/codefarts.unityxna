﻿using System;

namespace Microsoft.Xna.Framework.Input.Touch
{
    internal struct XNAINPUT_TOUCH_LOCATION_STATE
    {
        public int Count;

        public int Id0;

        public int Id1;

        public int Id2;

        public int Id3;

        public float X0;

        public float Y0;

        public float X1;

        public float Y1;

        public float X2;

        public float Y2;

        public float X3;

        public float Y3;
    }
}